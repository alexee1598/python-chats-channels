import logging

from channels.layers import get_channel_layer
from django.contrib.auth.hashers import check_password, make_password
from django.utils.http import urlsafe_base64_decode
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.response import Response
from rest_framework_simplejwt.views import TokenObtainPairView

from user.helper import registration, register_user_with_social, check_valid_email, _update_account, \
    _delete_account, _update_online_user
from user.models import SocialAuth, User
from user.serializers import UserSerializer

from user.serializers import LogInSerializer
from user.tasks import send_email_registration_job
from user.tokens import account_activation_token

logger = logging.getLogger(__name__)


class LogInView(TokenObtainPairView):
    serializer_class = LogInSerializer


@api_view(['POST'])
def register_user(request):
    try:
        user = registration(request)
    except Exception as e:
        if 'user_user_username_key' in str(e) or 'user_user.username' in str(e):
            return Response({
                'result': 0,
                'error': 'Username must be unique',
            })
        return Response({
            'result': 0,
            'error': 'Email address must be unique',
        })
    send_email_registration_job(user.id)
    if user is None:
        return Response({
            'result': 0,
            'error': 'Passwords must match'
        })
    return Response({
        'result': 1,
    })


@api_view(['POST'])
def social_register_user(request):
    try:
        social = SocialAuth.objects.get(provider=request.data['provider'], social_id=request.data['id'])
        user = User.objects.get(pk=social.user.id)
        _update_online_user(user)

        # token = Token.objects.get(user=user)
    except Exception as e:
        try:
            user = User.objects.get(email=request.data['email'])
            _update_online_user(user)
            register_user_with_social(request, user)

            # token = Token.objects.get(user=user)
        except User.DoesNotExist:
            user = registration(request)
            register_user_with_social(request, user)

            # token = Token.objects.create(user=user)

    return Response({
        'result': 1,
        # 'token': token.key,
        'user': UserSerializer(user).data,
    })


@api_view(['POST'])
def login(request):
    try:
        username = request.data['username']
        account = User.objects.get(username=username)
        password = check_password(request.data['password'], account.password)

        # token = Token.objects.get_or_create(user=account)

        if password:
            return Response({
                'user': UserSerializer(account).data,
                'result': 1,
                # 'token': TokenSerializer(token[0]).data,
            })
        else:
            return Response({
                "result": 0,
                "error": 'Incorrect Password',
            })

    except Exception as e:
        print(e)
        return Response({
            "result": 0,
            "error": 'Invalid Username',
        })


@api_view(['PUT', 'DELETE'])
@permission_classes([IsAuthenticated])
def account_view(request):
    account = request.user
    if request.method == 'PUT' and account != '':
        return _update_account(account, request)
    elif request.method == 'DELETE' and account != '':
        return _delete_account(account)


@api_view(['POST'])
@permission_classes([IsAuthenticated])
def change_password(request):
    account = request.user
    password = check_password(request.data['old_password'], account.password)
    if password:
        account.password = make_password(request.data['new_password'])
        account.save()
        return Response({
            "result": 1,
            "error": ''
        })
    else:
        return Response({
            "result": 0,
            "error": 'Incorrect password'
        })


@api_view(['POST'])
@permission_classes([IsAuthenticated])
def change_email(request):
    account = request.user
    password = check_password(request.data['password'], account.password)
    if password:
        check_valid_email(request.data['new_email'])
        account.email = request.data['new_email']
        account.save()
    else:
        return Response({
            "result": 0,
            "error": "Incorrect password"
        })


@api_view(['GET'])
@permission_classes([AllowAny])
def get_account(request, account_id):
    channel_layer = get_channel_layer()
    print(channel_layer)
    account = User.objects.get(id=account_id)

    return Response({
        "user": UserSerializer(account).data
    })


@api_view(['GET'])
def activate(request, uid, token):
    try:
        uid = urlsafe_base64_decode(uid).decode()
        acc = User.objects.get(pk=uid)
    except(TypeError, ValueError, OverflowError, User.DoesNotExist):
        acc = None

    if acc is not None and account_activation_token.check_token(acc, token):
        acc.is_active = True
        acc.save()
        return Response({'Activation link is valid!'})
    else:
        return Response({'Activation link is invalid!'})


@api_view(['GET'])
@permission_classes([AllowAny])
def get_context_data(self, **kwargs):
    res = send_email_registration_job()
    return Response({'result': res})
