from django.urls import path
from rest_framework_simplejwt.views import TokenRefreshView

from user import views
from user.views import LogInView


urlpatterns = [
    path('', views.get_context_data, name='index_view'),
    path('activate/<str:uid>/<str:token>', views.activate, name='activate_account'),
    path('update-account/', views.account_view, name='update_account'),
    path('change-email/', views.change_email, name='change_email'),
    path('change-password/', views.change_password, name='change_password'),
    path('delete-account/', views.account_view, name='delete_account'),
    path('login/', LogInView.as_view(), name='login'),
    path('log-in/', LogInView.as_view(), name='log_in'),
    path('token/refresh/', TokenRefreshView.as_view(), name='token_refresh'),
    path('sign-up/', views.register_user, name='registration'),
    path('social-register/', views.social_register_user, name='social_registration'),
    path('account/<int:account_id>', views.get_account, name='account_detail'),
]
