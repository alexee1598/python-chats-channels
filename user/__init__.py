from django.middleware import csrf


def get_or_create_csrf_token(request: 'request object'):
    token = request.META.get('CSRF_COOKIE', None)
    if token is None:
        token = csrf.get_token(request)
        request.META['CSRF_COOKIE'] = token
    request.META['CSRF_COOKIE_USED'] = True
    return token


default_app_config = 'chat.apps'
