from django.contrib.auth.models import AbstractUser
from django.db import models


class User(AbstractUser):
    photo = models.ImageField(upload_to='photos', null=True, blank=True)
    age = models.SmallIntegerField(null=True, blank=True)

    @property
    def group(self):
        groups = self.groups.all()
        return groups[0].name if groups else None


class SocialAuth(models.Model):
    account = models.ForeignKey(User, on_delete=models.CASCADE, related_name='socials')
    social_id = models.FloatField(unique=True)
    provider = models.CharField(max_length=20)
    created = models.DateTimeField(auto_now_add=True)

    class Meta:
        verbose_name_plural = 'SocialAuth'
        verbose_name = 'SocialsAuth'
