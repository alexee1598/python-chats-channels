import logging
import secrets
import string

from django.contrib.auth.hashers import make_password
from django.utils import timezone
from email_validator import validate_email, EmailNotValidError
from rest_framework.response import Response

from user.models import SocialAuth, User

logger = logging.getLogger(__name__)


def random_password():
    alphabet = string.ascii_letters + string.digits
    password = ''.join(secrets.choice(alphabet) for i in range(20))

    return password


def registration(request):
    u = User()
    u.email = request.data['email']
    u.username = request.data.get('username', '')
    u.age = request.data.get('age', )
    u.first_name = request.data.get('first_name', '')
    u.last_name = request.data.get('last_name', '')
    u.last_login = timezone.now()
    u.photo = request.data.get('photo', '')

    if request.data.get('password1') is not None:
        u.is_active = False
        if request.data['password1'] != request.data['password2']:
            return None
        u.password = make_password(request.data['password1'])
    else:
        u.first_name = request.data['firstName']
        u.last_name = request.data['lastName']
        u.username = request.data['email']
        u.password = make_password(random_password())

    u.save()
    return u


def register_user_with_social(request, user):
    social = SocialAuth()
    social.social_id = request.data['id']
    social.provider = request.data['provider']
    social.user = user
    social.save()


def check_valid_email(email):
    try:
        validate_email(email)

        return Response({
            "result": 1,
            "Error": ''
        })

    except EmailNotValidError as e:
        logger.exception(e)

    except Exception as e:
        logger.exception(e)

    return Response({
        "result": 0,
        "error": "It's invalid email domain"
    })


def _update_account(user, request):
    try:
        user.first_name = request.data['first_name']
        user.last_name = request.data['last_name']
        user.age = request.data['age']
        user.photo = request.data['photo']
        user.save()

        return Response({
            "result": 1,
            "Error": ''
        })

    except Exception as e:
        logger.exception(f'Update_account - {e}')

    return Response({
        "result": 0,
        "error": "Error update account"
    })


def _delete_account(request):
    try:
        a = User.objects.get(id=request.data['id'])
        a.delete()

        return Response({
            "result": 1,
            "Error": ''
        })

    except Exception as e:
        logger.exception(f'Delete_account - {e}')

    return Response({
        "result": 0,
        "error": "Error delete account"
    })


# def _get_account_by_token(request):
#     try:
#         token = (re.search('.+ (.+)', request.META.get('HTTP_AUTHORIZATION')))
#         return Account.objects.get(id=request.data['id'], auth_token=token.group(1))
#     except Account.DoesNotExist as e:
#         logger.exception(f'Account.DoesNotExist! {e}')
#     return ''


def _update_online_user(account):
    account.last_login = timezone.now()
    account.save()
