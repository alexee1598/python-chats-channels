from abc import ABC

from rest_framework import serializers
from rest_framework_simplejwt.serializers import TokenObtainPairSerializer

from user.models import User

#
# class UserSerializer(serializers.ModelSerializer):
#     group = serializers.CharField()
#
#     def create(self, data):
#         group_data = data.pop('group')
#         group, _ = Group.objects.get_or_create(name=group_data)
#         data = {key: value for key, value in data.items()}
#         data['password'] = data['password1']
#         user = self.Meta.model.objects.create_user(**data)
#         user.groups.add(group)
#         user.save()
#         return user
#
#     class Meta:
#         model = get_user_model()
#         fields = (
#             'id', 'username', 'password1', 'password2',
#             'first_name', 'last_name', 'group',
#             'photo',
#         )
#         read_only_fields = ('id',)


class UserSerializer(serializers.ModelSerializer):
    socials = serializers.StringRelatedField(many=True)

    class Meta:
        model = User
        fields = ('age', 'email', 'first_name', 'last_name', 'id', 'photo',
                  'username', 'is_active', 'socials')


class UserChatSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('first_name', 'last_name', 'username')


class LogInSerializer(TokenObtainPairSerializer):
    @classmethod
    def get_token(cls, user):
        token = super().get_token(user)
        user_data = UserSerializer(user).data
        for key, value in user_data.items():
            if key != 'id':
                token[key] = value
        return token
