from celery import shared_task
from django.core.mail import EmailMultiAlternatives
from django.template.loader import render_to_string
from django.utils.encoding import force_bytes
from django.utils.http import urlsafe_base64_encode

from pythonChannels.celery import app
from user.models import User
from user.tokens import account_activation_token


@shared_task
def add(x, y):
    return x + y


@shared_task
def mul(x, y):
    return x * y


@app.task
def send_email_registration_job(user_id: str) -> bool:
    try:
        mail_subject = 'Confirm your email address.'
        current_site = 'http://localhost:8000'
        user = User.objects.get(id=user_id)
        uid = urlsafe_base64_encode(force_bytes(user.pk))
        token = account_activation_token.make_token(user)
        activation_link = f"{current_site}/user/activate/{uid}/{token}"

        html_content = render_to_string('email/confirm-email.html',
                                        {'user': user,
                                         'activation_link': activation_link})
        text_content = 'This is an important message.'
        email = EmailMultiAlternatives(mail_subject, text_content, to=[user.email])
        email.attach_alternative(html_content, "text/html")
        email.send()
        return True
    except Exception as e:
        print(e)
        return False
