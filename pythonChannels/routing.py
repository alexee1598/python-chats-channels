from django.urls import path
from channels.routing import ProtocolTypeRouter, URLRouter

from chat.consumers import ChatConsumer
from pythonChannels.middleware import TokenAuthMiddlewareStack

application = ProtocolTypeRouter({
    'websocket': TokenAuthMiddlewareStack(
        URLRouter([
            path('chat/', ChatConsumer),
        ])
    ),
})
